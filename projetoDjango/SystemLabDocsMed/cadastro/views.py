from django.forms import ModelForm
from .models import *
from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect # Funcao para redirecionar o usuario
from django.contrib.auth.forms import UserCreationForm # Formulario de criacao de usuarios
from django.contrib.auth.forms import AuthenticationForm # Formulario de autenticacao de usuarios
from django.contrib.auth import login # funcao que salva o usuario na sessao


class UserForm(ModelForm):
    class Meta:
        model = Usuario
        fields = ['username', 'email', 'senha']

def index(request):
    return render(request, template_name="index.html")

def user_list(request, template_name='user_list.html'):
    usuario = Usuario.objects.all()
    usuarios = {'lista': usuario}
    return render(request, template_name, usuarios)

def user_new(request, template_name='new_user.html'):
    form = UserForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('user_list')
    return render(request, template_name, {'form': form})

def user_edit(request, pk, template_name='new_user.html'):
    usuario = get_object_or_404(Usuario, pk=pk)
    if request.method == "POST":
        form = UserForm(request.POST, instance=usuario)
        if form.is_valid():
            usuario = form.save()
            return redirect('user_list')
    else:
        form = UserForm(instance=usuario)
    return render(request, template_name, {'form': form})

def user_delete(request, pk):
    usuario = Usuario.objects.get(pk=pk)
    if request.method == 'POST':
        usuario.delete()
        return redirect('user_list')
    return render(request, 'user_delete.html', {'usuario': usuario})

def user_list(request, template_name="user_list.html"):
    query = request.GET.get("busca")
    if query:
      usuario = Usuario.objects.filter(username__icontains=query)
    else:
        usuario = Usuario.objects.all()
        usuarios = {'lista': usuario}
    return render(request, template_name, usuarios)



# pagina de login do usuário
def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST) # Veja a documentacao desta funcao
        
        if form.is_valid():
            #se o formulario for valido significa que o Django conseguiu encontrar o usuario no banco de dados
            #agora, basta logar o usuario e ser feliz.
            login(request, form.get_user())
            return HttpResponseRedirect("user_list.html") # redireciona o usuario logado para a pagina inicial
        else:
            return render(request, "login.html", {"form": form})
    
    #se nenhuma informacao for passada, exibe a pagina de login com o formulario
    return render(request, "login.html", {"form": AuthenticationForm()})
