from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^user_list/', user_list, name='user_list'),
    url(r'^user_new/', user_new, name='user_new'),
    url(r'^user_edit/(?P<pk>[1-9]+)', user_edit, name='user_edit'),
    url(r'^user_delete/(?P<pk>[1-9]+)', user_delete, name='user_delete'),
    url(r'^login/', login, name='login'),
    url(r'index', index, name='index'),
    
]
